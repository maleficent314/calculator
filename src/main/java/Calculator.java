public class Calculator {

    public int dodawanie(int x, int y){
        return x+y;
    }

    public int odejmowanie(int x, int y){
        return x-y;
    }

    public int mnozenie(int x, int y){
        return x*y;
    }

    public double dzielenie(int x, int y){
        if(y==0){
            System.out.println("Nie można dzielić przez 0.");
            return 0;
        }
        else {
            return (double) x / y;
        }
    }

    public double potegowanie(double x, double y){
        return Math.pow(x,y);
    }
}
